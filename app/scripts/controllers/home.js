'use strict';

/**
 * @ngdoc function
 * @name habilleToiApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the habilleToiApp
 */
angular.module('habilleToiApp')
  .controller('HomeCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
